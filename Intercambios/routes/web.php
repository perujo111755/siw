<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use App\Objeto;
use App\User;


/*Lo que cualquiera puede ver */
Route::get('/', 'IntercambioController@buscaIntercambios')->name('intercambiosget');
Route::post('/', 'IntercambioController@buscaIntercambios')->name('buscaIntercambios');//Todos los intercambios o intercambios filtrados

/*Lo que los usuarios pueden ver */
Route::middleware('auth')->group(function(){


/*Objetos*/
Route::get('/objeto','ObjetoController@verObj')->name('verObj');
Route::get('/verObjAjeno','ObjetoController@verObjAjeno')->name('verObjAjeno');
Route::get('/delete/{id}','ObjetoController@elimnarObj');
Route::get('/edit/{id}','ObjetoController@editarObj')->name('editarObj');
Route::put('/edit/{id}','ObjetoController@actualizarObj')->name('actualizarObj');
Route::get('/misobjetos', 'ObjetoController@misobjetos')->name('misobjetosget');
Route::post('/insertarObjeto', 'ObjetoController@insertarObjeto')->name('insertarObjeto');
Route::post('/visualizarObjetosIntercambio', 'ObjetoController@visualizarObjetosIntercambio')->name('visualizarObjetosIntercambio');
Route::post('/contraoferta', 'ObjetoController@contraoferta')->name('contraoferta');
Route::get('/nuevoobjeto','ObjetoController@nuevoobjeto')->name('nuevoobjeto');


/*Intercambios*/
Route::get('/crearintercambio', 'IntercambioController@crearintercambio')->name('crearintercambio');
Route::post('/crearContraoferta', 'IntercambioController@crearContraoferta')->name('crearContraoferta');
Route::get('/misIntercambiosAbiertos', 'IntercambioController@misIntercambiosAbiertos')->name('misIntercambiosAbiertos');
Route::get('/misIntercambiosEnviados', 'IntercambioController@misIntercambiosEnviados')->name('misIntercambiosEnviados');
Route::get('/misIntercambiosCerrados', 'IntercambioController@misIntercambiosCerrados')->name('misIntercambiosCerrados');
Route::post('/verIntercambio', 'IntercambioController@verIntercambio')->name('verIntercambio');
Route::post('/insertarIntercambio', 'IntercambioController@insertarIntercambio')->name('insertarIntercambio');
Route::post('/cerrarIntercambio', 'IntercambioController@cerrarIntercambio')->name('cerrarIntercambio');
Route::post('/eliminarIntercambio', 'IntercambioController@eliminarIntercambio')->name('eliminarIntercambio');
Route::get('/verIntercambioCerrado/{id}','IntercambioController@verIntercambioCerrado')->name('verIntercambioCerrado');
Route::get('/verIntercambioRecibido/{id}','IntercambioController@verIntercambioRecibido')->name('verIntercambioRecibido');
Route::get('/verIntercambioEnviado/{id}','IntercambioController@verIntercambioEnviado')->name('verIntercambioEnviado');
Route::get('/generarPDF/{id}','IntercambioController@generarPDF')->name('generarPDF');

/*Usuario*/
Route::post('/eliminarUsuario','UsuarioController@eliminarUsuario')->name('eliminarUsuario');
Route::post('/eliminarUsuario2','UsuarioController@eliminarUsuario2')->name('eliminarUsuario2');
Route::post('/hacerAdmin','UsuarioController@hacerAdmin')->name('hacerAdmin');
Route::get('/listaDeUsuarios','UsuarioController@listaDeUsuarios')->name('listaDeUsuarios');
Route::get('/perfil','UsuarioController@perfil')->name('perfil');
Route::get('/editarPerfil','UsuarioController@verEditarPerfil')->name('verEditarPerfil');
Route::post('/editarPerfil','UsuarioController@editarPerfil')->name('editarPerfil');
Route::get('/altaCSV','UsuarioController@altaCSV')->name('altaCSV');
Route::post('/altaMultiple' ,'UsuarioController@altaMultiple')->name('altaMultiple');
Route::get('/cambiarContraseña','UsuarioController@verCambiarContraseña');
Route::post('/cambiarContraseña','UsuarioController@cambiarContraseña')->name('cambiarContraseña');


/*Valoracion*/
Route::get('/crearvaloracion','ValoracionController@crearvaloracion')->name('crearvaloracion');
Route::get('/vervaloracion','ValoracionController@vervaloracion')->name('vervaloracion');
Route::post('/guardarvaloracion', 'ValoracionController@guardarvaloracion')->name('guardarvaloracion');

});

Auth::routes();
