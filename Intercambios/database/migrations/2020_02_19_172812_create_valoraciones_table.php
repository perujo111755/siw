<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValoracionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('final_valoraciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_int');
            $table->unsignedBigInteger('id_usr_1');
            $table->unsignedBigInteger('id_usr_2');
            $table->text('comentario')->nullable();
            $table->integer('puntuacion');
            $table->timestamps();

            
            //relaciones
            $table->foreign('id_int')->references('id')->on('final_intercambios_cerrados')->onDelete('cascade')->onUpdte('cascade');
            $table->foreign('id_usr_1')->references('id')->on('users')->onDelete('cascade')->onUpdte('cascade');
            $table->foreign('id_usr_2')->references('id')->on('users')->onDelete('cascade')->onUpdte('cascade');
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('valoraciones');
    }
}
