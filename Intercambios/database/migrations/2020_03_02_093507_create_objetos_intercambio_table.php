<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjetosIntercambioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('final_objetos_intercambio', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_int');
            $table->unsignedBigInteger('id_obj');
            $table->unsignedBigInteger('id_usr');
            $table->timestamps();

            
            //relaciones
            $table->foreign('id_int')->references('id')->on('final_intercambios_abiertos')->onDelete('cascade')->onUpdte('cascade');
            $table->foreign('id_obj')->references('id')->on('final_objetos')->onDelete('cascade')->onUpdte('cascade');
            $table->foreign('id_usr')->references('id')->on('users')->onDelete('cascade')->onUpdte('cascade');
        
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objetos_intercambio');
    }
}
