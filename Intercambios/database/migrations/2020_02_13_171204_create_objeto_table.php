<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjetoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('final_objetos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('propietario');    //guardamos el id del creador
            $table->string('nombre_obj',255);
            $table->string('estado',255);
            $table->text('descripcion');
            $table->timestamps();

            //relaciones
            $table->foreign('propietario')->references('id')->on('users')->onDelete('cascade')->onUpdte('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objetos');
    }
}
