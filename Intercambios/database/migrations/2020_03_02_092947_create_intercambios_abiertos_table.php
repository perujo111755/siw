<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIntercambiosAbiertosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('final_intercambios_abiertos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_usr_1');
            $table->unsignedBigInteger('id_usr_2');
            $table->timestamps();

            //relaciones
            $table->foreign('id_usr_1')->references('id')->on('users')->onDelete('cascade')->onUpdte('cascade');
            $table->foreign('id_usr_2')->references('id')->on('users')->onDelete('cascade')->onUpdte('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('intercambios_abiertos');
    }
}
