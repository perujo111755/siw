<?php

namespace App;

use Illuminate\Database\Eloquent\model;

class Objeto extends Model
{
    public $table = 'final_objetos';//nombre de la tabla en la bbdd

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $Objetos = [
        'id' , 'propietario','nombre_obj', 'estado', 'descripcion'
    ];

    public function fotos()
    {
        return $this->hasMany('App\Imagen');
    }

}
