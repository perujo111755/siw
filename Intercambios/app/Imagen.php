<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imagen extends Model
{
    public $table = 'final_imagenes';//nombre de la tabla en la bbdd
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $Imagenes = [
        'id', 'id_obj' ,'nombre_img', 'ruta'
    ];

}
