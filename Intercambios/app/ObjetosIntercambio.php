<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObjetosIntercambio extends Model
{
    public $table = 'final_objetos_intercambio';//nombre de la tabla en la bbdd
    //
    protected $ObjetosIntercambio = [
        'id', 'id_int', 'id_obj', 'id_usr'
    ];
}
