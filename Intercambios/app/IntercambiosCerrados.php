<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntercambiosCerrados extends Model
{
    public $table = 'final_intercambios_cerrados';//nombre de la tabla en la bbdd
    //
    protected $IntercambiosCerrados = [
        'id','id_usr_1', 'id_usr_2', 'ruta_json'
    ];
}
