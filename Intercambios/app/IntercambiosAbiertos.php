<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntercambiosAbiertos extends Model
{
    public $table = 'final_intercambios_abiertos';//nombre de la tabla en la bbdd
    //
    protected $IntercambiosAbiertos = [
        'id','id_usr_1', 'id_usr_2'
    ];
}
