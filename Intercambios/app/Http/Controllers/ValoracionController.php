<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Valoracion;
use DB;

class ValoracionController extends Controller
{
    /*Valoracion*/  
    function crearvaloracion() {
        return view('valoracion/crearvaloracion');
    }

    function vervaloracion() {
        $valoraciones = DB::table('final_valoraciones')->where('id_usr_2',auth()->user()->id)->get();
        return view('valoracion/vervaloracion',compact('valoraciones'));
    }


    function guardarvaloracion(Request $request) {
        $val = new Valoracion;
        $val->id_int = $request->input('id_int');
        if( !empty($_POST["star"])){
            $val->puntuacion = $_POST["star"];
        }else{
            return redirect()->route('misIntercambiosAbiertos')->with('error','Intercambio fallido');
        }
        $val->comentario = $request->input('comentario');
        $inter = DB::table('final_intercambios_cerrados')->where('id',$request->input('id_int'))->first();
        $val->id_usr_1 = auth()->user()->id;
        if(auth()->user()->id == $inter->id_usr_2){
            $val->id_usr_2 = $inter->id_usr_1;
        }else{
            $val->id_usr_2 = $inter->id_usr_2;
        }
        $val->save();
        return redirect()->route('misIntercambiosAbiertos')->with('info','Comentario enviado correctamente');
    }

}
