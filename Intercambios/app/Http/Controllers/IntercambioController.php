<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Objeto;
use App\Imagen;
use App\User;
use App\IntercambiosAbiertos;
use App\IntercambiosCerrados;
use App\ObjetosIntercambio;
use DB;
use Image;
use PDF;
use Auth;

class IntercambioController extends Controller
{
    /*Intercambios */
    function crearintercambio(){
        $objetos = DB::table('final_objetos')->where('final_objetos.propietario',auth()->user()->id)->select(DB::raw('final_objetos.nombre_obj, final_objetos.id,final_objetos.descripcion,final_objetos.estado,(select ruta from final_imagenes where final_imagenes.id_obj = final_objetos.id LIMIT 1) as ruta'))->get();
        return view('intercambios/crearintercambio',compact('objetos'));
    }

    

    function misIntercambiosAbiertos(){
         $intercambios = DB::table('final_intercambios_abiertos')->select(DB::raw('final_intercambios_abiertos.updated_at,final_intercambios_abiertos.id, final_intercambios_abiertos.id_usr_1, final_intercambios_abiertos.id_usr_2, (select email from users where users.id = final_intercambios_abiertos.id_usr_1 LIMIT 1) as email'))->where('id_usr_2',auth()->user()->id)->get();
        return view('intercambios/misIntercambiosAbiertos',compact('intercambios'));
    }

    function misIntercambiosEnviados(){
        $intercambios = DB::table('final_intercambios_abiertos')->select(DB::raw('final_intercambios_abiertos.updated_at,final_intercambios_abiertos.id, final_intercambios_abiertos.id_usr_1, final_intercambios_abiertos.id_usr_2, (select email from users where users.id = final_intercambios_abiertos.id_usr_2 LIMIT 1) as email'))->where('id_usr_1',auth()->user()->id)->get();
        return view('intercambios/misIntercambiosEnviados',compact('intercambios'));
    }
    
    function misIntercambiosCerrados(){
        $intercambios = DB::table('final_intercambios_cerrados')->select(DB::raw('final_intercambios_cerrados.updated_at,final_intercambios_cerrados.id, final_intercambios_cerrados.id_usr_1, final_intercambios_cerrados.id_usr_2, (select email from users where users.id = final_intercambios_cerrados.id_usr_1 LIMIT 1) as email'))->where('id_usr_2',auth()->user()->id)->orWhere('id_usr_1',auth()->user()->id)->get();
        return view('intercambios/misIntercambiosCerrados',compact('intercambios'));
    }
    
    function cerrarIntercambio(Request $request){
        $inter = new IntercambiosCerrados;
        $inter->id_usr_1 = $request->input('id_usr_1');
        $inter->id_usr_2 = $request->input('id_usr_2');
        
        //Crear JSON
        $auxDatosIntercambio=DB::table('final_intercambios_abiertos')->where('id',$request->input('id'))->get();
        $auxDatosUsuario1=DB::table('users')->where('id',$inter->id_usr_1)->first();
        $auxDatosUsuario2=DB::table('users')->where('id',$inter->id_usr_2)->first();
        $auxObjetos1=DB::table('final_objetos_intercambio')->where('id_int',$request->input('id'))->Where('id_usr',$inter->id_usr_1)->get();
        $auxObjetos2=DB::table('final_objetos_intercambio')->where('id_int',$request->input('id'))->Where('id_usr',$inter->id_usr_2)->get();
        $ob1=json_decode(json_encode($auxObjetos1), true);
        $ob2=json_decode(json_encode($auxObjetos2), true);
        // borrar objetOS
        $objetos1= array();
        $i=0;
        foreach($ob1 as $ob){
            $objeto=Objeto::findOrFail($ob["id_obj"]);
            $auxRuta=DB::table('final_imagenes')->select('ruta')->where('id_obj',$ob["id_obj"])->first();
            $auxRuta=json_decode(json_encode($auxRuta),true);
            $objeto->ruta=$auxRuta['ruta'];
            $objetos1[$i]=$objeto;
            DB::table('final_objetos')->where('id',$ob["id_obj"])->delete();
            $i++;
        }
        $objetos2= array();
        $i=0;
        foreach($ob2 as $ob){
            $objeto=Objeto::findOrFail($ob["id_obj"]);
            $auxRuta=DB::table('final_imagenes')->select('ruta')->where('id_obj',$ob["id_obj"])->first();
            $auxRuta=json_decode(json_encode($auxRuta),true);
            $objeto->ruta=$auxRuta['ruta'];
            $objetos2[$i]=$objeto;
            DB::table('final_objetos')->where('id',$ob["id_obj"])->delete();
            $i++;
        }
        $AuxJson = array(
            "usuario1" => array("nombre1"=>$auxDatosUsuario1->name, "email1"=>$auxDatosUsuario1->email, "id1"=>$auxDatosUsuario1->id),
            "usuario2" => array("nombre2"=>$auxDatosUsuario2->name, "email2"=>$auxDatosUsuario2->email, "id2"=>$auxDatosUsuario2->id),
            "objetos1" => $objetos1,
            "objetos2" => $objetos2
        );
        $id=uniqid() . ".json";
        $json= json_encode($AuxJson);
        $handler= fopen(public_path("json/" . $id),"w+" );
        fwrite($handler,$json);
        fclose($handler);
        $inter->ruta_json = public_path("json/" . $id);
        $inter->save(); 
   
        //borrar intercambio abierto
        DB::table('final_intercambios_abiertos')->where('id',$request->input('id'))->delete();

        $id_int = $inter->id;
        return view('valoracion/crearvaloracion',compact('id_int'))->with('info','Intercambio aceptado correctamente');
    }

    function eliminarIntercambio(Request $request){
        DB::table('final_intercambios_abiertos')->where('id',$request->input('id'))->delete();
        return redirect()->route('misIntercambiosAbiertos')->with('info','Intercambio rechazado correctamente');
    }

    function insertarIntercambio(Request $request){
        //guardamos objeto
        $int = new IntercambiosAbiertos;
        $int->id_usr_1 = auth()->user()->id;
        $aux = $request->input('id_usr');
        $aux = str_replace('[', '', $aux);
        $aux = str_replace(']', '', $aux);
        $int->id_usr_2 = $aux; // el 0 es el 1º corchete y el 1 ya es el valor y el 2 el corchete que cierra
        if($_POST["submit"]){
            if( empty($_POST["check_list"]) or empty($_POST["check_list_1"])){
                return redirect()->route('intercambiosget')->with('error','Intercambio fallido');
            }else{
                $int->save();
                foreach ($_POST["check_list"] as $check) {
                    $obj_int = new ObjetosIntercambio;
                    $obj_int->id_int = $int->id;
                    $obj_int->id_obj = $check;
                    $obj_int->id_usr = $int->id_usr_1;
                    $obj_int->save();
                }
                foreach ($_POST["check_list_1"] as $check) {
                    $obj_int = new ObjetosIntercambio;
                    $obj_int->id_int = $int->id;
                    $obj_int->id_obj = $check;
                    $obj_int->id_usr = $int->id_usr_2;
                    $obj_int->save();
                }
            }
        }
        return redirect()->route('intercambiosget')->with('info','Intercambio creado correctamente');
    }

    function crearContraoferta(Request $request){
        //insertarIntercambio
        $int = new IntercambiosAbiertos;
        $int->id_usr_1 = auth()->user()->id;
        $aux = $request->input('id_usr');
        $aux = str_replace('[', '', $aux);
        $aux = str_replace(']', '', $aux);
        $int->id_usr_2 = $aux; // el 0 es el 1º corchete y el 1 ya es el valor y el 2 el corchete que cierra
        if($_POST["submit"]){
            if( empty($_POST["check_list"]) or empty($_POST["check_list_1"])){
                return redirect()->route('intercambiosget')->with('error','Intercambio fallido');
            }else{
                $int->save();
                foreach ($_POST["check_list"] as $check) {
                    $obj_int = new ObjetosIntercambio;
                    $obj_int->id_int = $int->id;
                    $obj_int->id_obj = $check;
                    $obj_int->id_usr = $int->id_usr_1;
                    $obj_int->save();
                }
                foreach ($_POST["check_list_1"] as $check) {
                    $obj_int = new ObjetosIntercambio;
                    $obj_int->id_int = $int->id;
                    $obj_int->id_obj = $check;
                    $obj_int->id_usr = $int->id_usr_2;
                    $obj_int->save();
                }
            }
        }
        //eliminarIntercambio
        DB::table('final_intercambios_abiertos')->where('id',$request->input('id'))->delete();
        return redirect()->route('misobjetosget')->with('info','Contraoferta creada correctamente');
    }


    function todosIntercambios(){
        $objetos = DB::table('final_objetos')->select(DB::raw('final_objetos.nombre_obj, final_objetos.id,final_objetos.descripcion,final_objetos.estado,(select ruta from final_imagenes where final_imagenes.id_obj = final_objetos.id LIMIT 1) as ruta,
                                                                                                                                                      (select email from users where users.id = final_objetos.propietario LIMIT 1) as email'))->get();
        return view('intercambios/listaImagenes',compact('objetos'));
    }


    function buscaIntercambios(Request $request){
        $busqueda = $request->input('buscador');
        if(Auth::user()){
            $objetos = DB::table('final_objetos')
            ->select(DB::raw('final_objetos.nombre_obj, final_objetos.id,final_objetos.descripcion,final_objetos.estado,(select ruta from final_imagenes where final_imagenes.id_obj = final_objetos.id LIMIT 1) as ruta, (select email from users where users.id = final_objetos.propietario LIMIT 1) as email'))
            ->where('final_objetos.propietario','!=',auth()->user()->id)
            ->where('final_objetos.nombre_obj', 'like', '%'.$busqueda.'%')->get();
        }else{
            $objetos = DB::table('final_objetos')
            ->select(DB::raw('final_objetos.nombre_obj, final_objetos.id,final_objetos.descripcion,final_objetos.estado,(select ruta from final_imagenes where final_imagenes.id_obj = final_objetos.id LIMIT 1) as ruta,(select email from users where users.id = final_objetos.propietario LIMIT 1) as email'))
            ->where('final_objetos.nombre_obj', 'like', '%'.$busqueda.'%')->get();
        }
        return view('intercambios/listaImagenes',compact('objetos'))->with('busqueda',$busqueda);
    }


    function verIntercambioCerrado($id){
        $intercambio=IntercambiosCerrados::findOrFail($id);
        $data = file_get_contents($intercambio->ruta_json);
        $products = json_decode($data, true);
        //info usuario1
        $usuario=new \stdClass();
        $usuario->id=$products["usuario1"]["id1"];
        $usuario->name=$products["usuario1"]["nombre1"];
        $usuario->email=$products["usuario1"]["email1"];
        //info usuario2
        $usuario2=new \stdClass();
        $usuario2->id=$products["usuario2"]["id2"];
        $usuario2->name=$products["usuario2"]["nombre2"];
        $usuario2->email=$products["usuario2"]["email2"];
        //info objetos1
        $objetos1=array();
        $i=0;
        foreach($products["objetos1"] as $ob1){
            $objeto= new \stdClass();
            $objeto->nombre_obj=$ob1["nombre_obj"];
            $objeto->ruta=$ob1["ruta"];
            $objeto->estado=$ob1["estado"];
            $objetos1[$i]=$objeto;
            $i++;
        }
        //info objetos2
        $objetos2=array();
        $i=0;
        foreach($products["objetos2"] as $ob1){
            $objeto= new \stdClass();
            $objeto->nombre_obj=$ob1["nombre_obj"];
            $objeto->ruta=$ob1["ruta"];
            $objeto->estado=$ob1["estado"];
            $objetos2[$i]=$objeto;
            $i++;
        }
       return view('intercambios/verIntercambioCerrado', compact('usuario'))->with('intercambio',$intercambio)->with('usuario2',$usuario2)->with('objetos1',$objetos1)->with('objetos2',$objetos2);
    }
    function verIntercambioRecibido($id){
        $intercambio=IntercambiosAbiertos::findOrFail($id);
        //var_dump($intercambio);
        
        //info usuario1
        $usuario=User::findOrFail($intercambio->id_usr_1);
        $objetos1=array();
        $i=0;
        $auxObjetos1=DB::table('final_objetos_intercambio')->where('id_int',$id)->Where('id_usr',$intercambio->id_usr_1)->get();
        $auxObjetos2=DB::table('final_objetos_intercambio')->where('id_int',$id)->Where('id_usr',$intercambio->id_usr_2)->get();
        $ob1=json_decode(json_encode($auxObjetos1), true);
        $ob2=json_decode(json_encode($auxObjetos2), true);
        $objetos1= array();
        $i=0;
        foreach($ob1 as $ob){
            $objeto=Objeto::findOrFail($ob["id_obj"]);
            $auxRuta=DB::table('final_imagenes')->select('ruta')->where('id_obj',$ob["id_obj"])->first();
            $auxRuta=json_decode(json_encode($auxRuta),true);
            $objeto->ruta=$auxRuta['ruta'];
            $objetos1[$i]=$objeto;
            $i++;
        }
        $objetos2= array();
        $i=0;
        foreach($ob2 as $ob){
            $objeto=Objeto::findOrFail($ob["id_obj"]);
            $auxRuta=DB::table('final_imagenes')->select('ruta')->where('id_obj',$ob["id_obj"])->first();
            $auxRuta=json_decode(json_encode($auxRuta),true);
            $objeto->ruta=$auxRuta['ruta'];
            $objetos2[$i]=$objeto;
            $i++;
        }
       return view('intercambios/verIntercambioRecibido', compact('usuario'))->with('intercambio',$intercambio)->with('objetos1',$objetos1)->with('objetos2',$objetos2);
    }

    function verIntercambioEnviado($id){
        $intercambio=IntercambiosAbiertos::findOrFail($id);
        //var_dump($intercambio);
        
        //info usuario1
        $usuario=User::findOrFail($intercambio->id_usr_2);
        $objetos1=array();
        $i=0;
        $auxObjetos1=DB::table('final_objetos_intercambio')->where('id_int',$id)->Where('id_usr',$intercambio->id_usr_1)->get();
        $auxObjetos2=DB::table('final_objetos_intercambio')->where('id_int',$id)->Where('id_usr',$intercambio->id_usr_2)->get();
        $ob1=json_decode(json_encode($auxObjetos1), true);
        $ob2=json_decode(json_encode($auxObjetos2), true);
        $objetos1= array();
        $i=0;
        foreach($ob1 as $ob){
            $objeto=Objeto::findOrFail($ob["id_obj"]);
            $auxRuta=DB::table('final_imagenes')->select('ruta')->where('id_obj',$ob["id_obj"])->first();
            $auxRuta=json_decode(json_encode($auxRuta),true);
            $objeto->ruta=$auxRuta['ruta'];
            $objetos1[$i]=$objeto;
            $i++;
        }
        $objetos2= array();
        $i=0;
        foreach($ob2 as $ob){
            $objeto=Objeto::findOrFail($ob["id_obj"]);
            $auxRuta=DB::table('final_imagenes')->select('ruta')->where('id_obj',$ob["id_obj"])->first();
            $auxRuta=json_decode(json_encode($auxRuta),true);
            $objeto->ruta=$auxRuta['ruta'];
            $objetos2[$i]=$objeto;
            $i++;
        }
       return view('intercambios/verIntercambioEnviado', compact('usuario'))->with('intercambio',$intercambio)->with('objetos1',$objetos1)->with('objetos2',$objetos2);
    }
        function generarPDF($id){
        $pdf= resolve('dompdf.wrapper');
        $intercambio=IntercambiosCerrados::findOrFail($id);
        $data = file_get_contents($intercambio->ruta_json);
        $products = json_decode($data, true);
        //info usuario1
        $usuario=new \stdClass();
        $usuario->id=$products["usuario1"]["id1"];
        $usuario->name=$products["usuario1"]["nombre1"];
        $usuario->email=$products["usuario1"]["email1"];
        //info usuario2
        $usuario2=new \stdClass();
        $usuario2->id=$products["usuario2"]["id2"];
        $usuario2->name=$products["usuario2"]["nombre2"];
        $usuario2->email=$products["usuario2"]["email2"];
        //info objetos1
        $objetos1=array();
        $i=0;
        foreach($products["objetos1"] as $ob1){
            $objeto= new \stdClass();
            $objeto->nombre_obj=$ob1["nombre_obj"];
            $objeto->ruta=$ob1["ruta"];
            $objeto->estado=$ob1["estado"];
            $objetos1[$i]=$objeto;
            $i=$i+1;
        }
        //info objetos2
        $objetos2=array();
        $i=0;
        foreach($products["objetos2"] as $ob1){
            $objeto= new \stdClass();
            $objeto->nombre_obj=$ob1["nombre_obj"];
            $objeto->ruta=$ob1["ruta"];
            $objeto->estado=$ob1["estado"];
            $objetos2[$i]=$objeto;
            $i=$i+1;
        }
        $pdf= resolve('dompdf.wrapper');
        $data2=array('usuario'=>$usuario,'usuario2'=>$usuario2,'objetos1'=>$objetos1,'objetos2'=>$objetos2);
        $pdf= \PDF::loadView('pdf',$data2);
        $pdf->stream();
        return $pdf->download("Intercambio.pdf");
    }

}