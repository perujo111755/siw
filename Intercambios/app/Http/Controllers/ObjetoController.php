<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Objeto;
use App\Imagen;
use App\Intercambio;
use DB;
use Image;


class ObjetoController extends Controller
{
    /*Objetos */
    function insertarObjeto(Request $req){
        //guardamos objeto
        $obj = new Objeto;
        $obj->nombre_obj = $req->input('nombre');
        $obj->estado = $req->input('estado');
        $obj->descripcion = $req->input('descripcion');
        $obj->propietario = auth()->user()->id;
        $obj->save();
        if($imagen = $req->file('imagen')){
            foreach ($imagen as $imag) {
                //guardamos la mediana
                $nombreimagen = $imag->getClientOriginalName();
                list($anch, $alt) = getimagesize($imag);
                $image_resize = Image::make($imag->getRealPath());              
                $image_resize->resize($anch/2, $alt/2);
                $image_resize->save(public_path('images/medianas/' .$nombreimagen));
                //guardamos la pequena
                $image_resize2 = Image::make($imag->getRealPath());              
                $image_resize2->resize($anch/4, $alt/4);
                $image_resize2->save(public_path('images/pequenas/' .$nombreimagen));
                //guardamos la imagen grande
                if($imag->move('images/grandes/',$nombreimagen)){
                    $img = new Imagen;
                    $img->ruta = $nombreimagen;
                    $img->nombre_img = $nombreimagen;
                    $img->id_obj = $obj->id;
                    $img->save();
                }
            }
        }
        return redirect()->route('misobjetosget')->with('info','Objeto creado correctamente');
    }

    function nuevoobjeto(){
        return view('objetos/nuevoobjeto');
    }
    

    function elimnarObj($id){
        DB::table('final_objetos')->where('id',$id)->delete();
        return redirect()->route('misobjetosget')->with('info','Objeto eliminado correctamente');
    }

    function misobjetos(){
        $objetos = DB::table('final_objetos')->where('final_objetos.propietario',auth()->user()->id)->select(DB::raw('final_objetos.nombre_obj, final_objetos.id,final_objetos.descripcion,final_objetos.estado,(select ruta from final_imagenes where final_imagenes.id_obj = final_objetos.id LIMIT 1) as ruta'))->get();
        return view('objetos/misobjetos',compact('objetos'));
    }

    function visualizarObjetosIntercambio(Request $req){
        $id_usr = DB::table('final_objetos')->where('id',$req->input('id'))->pluck('propietario');
        $objetos = DB::table('final_objetos')->where('final_objetos.propietario',auth()->user()->id)->select(DB::raw('final_objetos.nombre_obj, final_objetos.id,final_objetos.descripcion,final_objetos.estado,(select ruta from final_imagenes where final_imagenes.id_obj = final_objetos.id LIMIT 1) as ruta'))->get();
        $objetosAjenos = DB::table('final_objetos')->where('final_objetos.propietario',$id_usr)->select(DB::raw('final_objetos.nombre_obj, final_objetos.id,final_objetos.descripcion,final_objetos.estado,(select ruta from final_imagenes where final_imagenes.id_obj = final_objetos.id LIMIT 1) as ruta'))->get();
        return view('intercambios/crearintercambio',compact('objetos'))->with('objetosAjenos',$objetosAjenos)->with('id_usr',$id_usr);
    }


    function contraoferta(Request $req){
        $id_usr = $req->input('id_usr_1');
        $id = $req ->input('id');
        $objetos = DB::table('final_objetos')->where('final_objetos.propietario',auth()->user()->id)->select(DB::raw('final_objetos.nombre_obj, final_objetos.id,final_objetos.descripcion,final_objetos.estado,(select ruta from final_imagenes where final_imagenes.id_obj = final_objetos.id LIMIT 1) as ruta'))->get();
        $objetosAjenos = DB::table('final_objetos')->where('final_objetos.propietario',$id_usr)->select(DB::raw('final_objetos.nombre_obj, final_objetos.id,final_objetos.descripcion,final_objetos.estado,(select ruta from final_imagenes where final_imagenes.id_obj = final_objetos.id LIMIT 1) as ruta'))->get();
        return view('intercambios/contraoferta',compact('objetos'))->with('objetosAjenos',$objetosAjenos)->with('id_usr',$id_usr)->with('id',$id);
    }

    
    function editarObj($id){
        $objeto = Objeto::findOrFail($id);
        return view('objetos/editaobjeto',compact('objeto'));
    }

    function verObj(Request $req){
        $objeto = DB::table('final_objetos')->select(DB::raw(
            'final_objetos.nombre_obj, final_objetos.id,final_objetos.descripcion,final_objetos.estado,(select ruta from final_imagenes where final_imagenes.id_obj = final_objetos.id LIMIT 1) as ruta'))
            ->where('final_objetos.id', $req->input('id'))->first();
        $imagenes=DB::table('final_imagenes')->where('final_imagenes.id_obj',$req->input('id'))->get();
        return view('objetos/objetocompleto',compact('objeto'))->with('imagenes',$imagenes);
    }

    function verObjAjeno(Request $req){
        $objeto = DB::table('final_objetos')->select(DB::raw(
            'final_objetos.nombre_obj, final_objetos.id,final_objetos.descripcion,final_objetos.estado,(select ruta from final_imagenes where final_imagenes.id_obj = final_objetos.id LIMIT 1) as ruta'))
            ->where('final_objetos.id', $req->input('id'))->first();
        $imagenes=DB::table('final_imagenes')->where('final_imagenes.id_obj',$req->input('id'))->get();
        return view('objetos/objetoCompletoAjeno',compact('objeto'))->with('imagenes',$imagenes);
    }

    function actualizarObj(Request $req, $id){
        $objeto = Objeto::findOrFail($id);
        $objeto->nombre_obj = $req->input('nombre');
        $objeto->estado = $req->input('estado');
        $objeto->descripcion = $req->input('descripcion');
        if($imagen = $req->file('imagen')){
            DB::table('final_imagenes')->where('id_obj',$id)->delete();
            foreach ($imagen as $imag) {
                //guardamos la mediana
                $nombreimagen = $imag->getClientOriginalName();
                list($anch, $alt) = getimagesize($imag);
                $image_resize = Image::make($imag->getRealPath());              
                $image_resize->resize($anch/2, $alt/2);
                $image_resize->save(public_path('images/medianas/' .$nombreimagen));
                //guardamos la pequena
                $image_resize2 = Image::make($imag->getRealPath());              
                $image_resize2->resize($anch/4, $alt/4);
                $image_resize2->save(public_path('images/pequenas/' .$nombreimagen));
                //guardamos la imagen grande
                if($imag->move('images/grandes/',$nombreimagen)){
                    $img = new Imagen;
                    $img->ruta = $nombreimagen;
                    $img->nombre_img = $nombreimagen;
                    $img->id_obj = $id;
                    $img->save();
                }
            }
        }
        $objeto->save();
        return redirect()->route('misobjetosget')->with('info','Objeto actualizado correctamente');
    }
    
}
