<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use DB;
use Hash;
use Auth;

class UsuarioController extends Controller
{
    /*Usuario */
    function eliminarUsuario(Request $req){
        $id = $req->input('id');
        $user = User::findOrFail($id);
        $user->delete();
        return view('/');
    }
    
    function eliminarUsuario2(Request $req){
        $id = $req->input('id');
        $user = User::findOrFail($id);
        $user->delete();
        $users=DB::table('users')->get();
        return redirect()->route('listaDeUsuarios', compact('users'))->with('info','Usuario eliminado correctamente');
    }

    function listaDeUsuarios(){
        $users=DB::table('users')->get();
        return view('gestionarUsuarios', compact('users'));
    }

    function hacerAdmin(Request $req){
        $id = $req->input('id');
        $user = User::findOrFail($id);
        if($user->tipo==1){
            $user->tipo=0;
            $user->save();
            $users=DB::table('users')->get();
            return redirect()->route('listaDeUsuarios', compact('users'))->with('info','Usuario eliminado de administrador correctamente');
        }else{
            $user->tipo= 1;
            $user->save();
            $users=DB::table('users')->get();
            return redirect()->route('listaDeUsuarios', compact('users'))->with('info','Usuario promocionado a administrador correctamente');
        }
        
    }

    function perfil(){
        return view('perfil/perfil');
    }

    function verCambiarContraseña(){
        return view('perfil/cambiarContraseña');
    }

    function cambiarContraseña(Request $request){
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {// Las contraseñas coinciden
            return redirect()->back()->with("error","La constraseña actual no coincide con la contraseña dada, por favor intentelo de nuevo");
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){// Las contraseña vieja y nueva son la misma
            return redirect()->back()->with("error","La nueva contraseña no puede ser igual a la actual, por favor elija una diferente");
        }
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);
        //Cambiar contraseña
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return redirect()->back()->with("success","Contraseña cambiada correctamente!");
    }

    function verEditarPerfil(){
        return view('perfil/editarPerfil');
    }

    function editarPerfil(Request $request){
        $user = Auth::user();
        $user->name = $request->input('nombre');
        $user->email = $request->input('correo');
        if($user->name=="" || $user->email==""){
            return redirect()->back()->with("error","Error nombre o correo no puede estar vacio");
        }

        $user->save();
        return redirect()->route('perfil')->with("success","Perfil cambiado correctamente!");
    }

    function altaCSV(){
        $users=DB::table('users')->get();
        $file=fopen("usuarios.json","w+");
        $contenido=json_encode($users);
        fwrite($file,$contenido);
        fclose($file);
        return view('altaUsuariosCSV');
    }
    function altaMultiple(Request $request){
        $upload=$request->file('file');
        $filePath=$upload->getRealPath();
        $file=fopen($filePath,'r');
        while (!feof($file)) {
            $data=fgetcsv($file);
            $user=User::create([
                'name'=> $data[0],
                'email'=>$data[1],
                'password'=>bcrypt($data[2])
            ]);

        }
        $users=DB::table('users')->get();
        return view('gestionarUsuarios', compact('users'));
    }

}
