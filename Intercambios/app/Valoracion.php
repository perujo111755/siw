<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Valoracion extends Model
{
    public $table = 'final_valoraciones';//nombre de la tabla en la bbdd
    //
    protected $Valoraciones = [
        'id','id_int', 'id_usr_1', 'id_usr_2', 'comentario', 'puntuacion'
    ];
}
