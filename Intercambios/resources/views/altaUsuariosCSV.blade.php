@extends('layouts.mizona')
@section('contenido')

        <div class="container">

        <div class="card mb-3" >
        <div class="card-header">
            Importar usuarios mediante un archivo CSV
        </div>
            <form id="altaCSV" enctype="multipart/form-data" method="post" action="/altaMultiple">
                 @csrf
                CSV File:<input type="file" name="file" id="file">
                <input type="submit" value="Enviar" name="enviar">
            </form>
        </div>
        <br></br>
        <div class="card mb-3" >
        <div class="card-header">
            Exportar usuarios a un archivo JSON
        </div>
            <a href="usuarios.json" class='btn' download="usuarios.json">Descargar archivo</a>
        </div>
        </div>
 @endsection