@extends('layouts.mizona')
@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Crear Objeto
                    </div>
                    <div class="card-body">
                        <form id= "insertarObjeto" action="{{ route('insertarObjeto')}}" method = "POST" enctype = "multipart/form-data">
                        @csrf
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default">Nombre</span>
                                </div>
                                <input type="string" name="nombre" required class="form-control">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01">Estado</label>
                                </div>
                                <select name="estado" class="custom-select" id="estado">
                                    <option value="En su caja original">En su caja original</option>
                                    <option value="Con algun rasguño">Con algun rasguño</option>
                                    <option selected value="Casi nuevo">Casi nuevo</option>
                                    <option value="Deplorable">Deplorable</option>
                                </select>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default">Breve descripcion</span>
                                </div>
                                <textarea name="descripcion" class="form-control rounded-0" id="exampleFormControlTextarea1" rows="10"></textarea>
                            </div>
                            <input class="dropzone" type="file" name="imagen[]" id = "imagen" multiple /> <br><br>
                            <button class="btn btn-primary" type="submit">Crear</button>
                            <a href="{{ route ('misobjetosget')}}" class="btn btn-danger">Cancelar</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection