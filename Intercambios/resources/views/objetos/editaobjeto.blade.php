@extends('layouts.mizona')
@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Editar Objeto
                    </div>
                    <div class="card-body">
                        <form action="{{route('actualizarObj', $objeto->id)}}" method = "POST" enctype = "multipart/form-data">
                        @method('put')
                        @csrf
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default">Nombre</span>
                                </div>
                                <input type="string" name="nombre" required class="form-control" value = "{{$objeto->nombre_obj}}">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01">Estado</label>
                                </div>
                                <select name="estado" class="custom-select" id="estado" value = "{{$objeto->estado}}">
                                    <option value="En su caja original">En su caja original</option>
                                    <option value="Con algun rasguño">Con algun rasguño</option>
                                    <option selected value="Casi nuevo">Casi nuevo</option>
                                    <option value="Deplorable">Deplorable</option>
                                </select>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default" >Breve descripcion</span>
                                </div>
                                <textarea name="descripcion" class="form-control" id="exampleFormControlTextarea1" rows="10">{{$objeto->descripcion}}</textarea>
                            </div>
                            <input class="dropzone" type="file" name="imagen[]" id = "imagen" multiple /> <br><br>
                            <button class="btn btn-primary" type="submit">Guardar</button>
                            <a href="{{route('misobjetosget')}}" class="btn btn-danger">Cancelar</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
