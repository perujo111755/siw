@extends('layouts.mizona')
@section('contenido')
    <link rel="stylesheet" href="css/vistaImagenes.css" />
    <link rel="stylesheet" href="css/lc_lightbox.css" />
    <link rel="stylesheet" href="skins/minimal.css" />
    
    <!-- Tienen que estar en el head -->
    <script src="lib/AlloyFinger/alloy_finger.min.js" type="text/javascript"></script>
    <script src="lib/jquery.js" type="text/javascript"></script>
    <script src="js/lc_lightbox.lite.js" type="text/javascript"></script>
    <script src="/js/verImagenes.js" type="text/javascript"></script>

    <div class="card mb-3" >
				<div class="card-header">
                    Imagenes
				</div>
				<div class="card-body">
                    @foreach($imagenes  as $imagen)
                        <a class="elem" href="images/grandes/{{$imagen->ruta}}" data-lcl-thumb="images/pequenas/{{$imagen->ruta}}" style="width: 18rem; display: inline-block;">
                            <span style="background-image: url(images/medianas/{{$imagen->ruta}});"></span>
                        </a>
                    @endforeach
				</div>
                <div class="card-footer">
                    Mas informacion
				</div>
				<div class="card-body">
                    <h5 id='nombre' class="card-title">Nombre: {{$objeto->nombre_obj}}</h5>
                    <p id='estado' class="card-text">Estado: {{$objeto->estado}}</p>
                    <p id='descripcion' class="card-text">Descripcion: {{$objeto->descripcion}}</p>
				</div>
    </div>
@endsection
