<!DOCTYPE html>
<html>
	<head>
		<title>Generate Pdf</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<style>
            table{ border: 1px solid black;} th, td { padding: 10px;} table{margin: 0 auto;}
            .padre {text-align: center;}	
        </style>
	</head>
	<body>
		<link rel="stylesheet" href="css/vistaImagenes.css" />
		<link rel="stylesheet" href="css/lc_lightbox.css" />
		<link rel="stylesheet" href="skins/minimal.css" />

        <div class="padre">
            <div>
                <H1>Resumen de intercambio</H1>
            </div>
            <div >
                <H2>Oferta realizada por:</H2>
                <table border: 1px solid black;>
                    <thead>
                        <tr>
                            <th scope="col">NOMBRE</th>
                            <th scope="col">EMAIL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$usuario->name}}</td>
                            <td>{{$usuario->email}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div>
                <H2> Oferta aceptada por:</H2>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">NOMBRE</th>
                            <th scope="col">EMAIL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$usuario2->name}}</td>
                            <td>{{$usuario2->email}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div >
                <H2>Objetos intercambiados por el usuario 1</H2>
                <H1></H1>
                <table >
                    <tr>
                        <th scope="col">Objeto</th>
                        <th scope="col">Descrpcion</th>
                        <th scope="col">Imagen</th>
                    </tr>
                    </thead>
                    @foreach($objetos1 as $objeto)
                    <tbody>
                        <tr>
                            <td>{{$objeto->nombre_obj}}</td>
                            <td>{{$objeto->estado}}</td>
                            <td><img src="./images/medianas/{{$objeto->ruta}}" height="150" width="150"></td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
            <div >
                <H2>Objetos intercambiados por el usuario 2</H2>
                <H1></H1>
                <table >
                    <tr>
                        <th scope="col">Objeto</th>
                        <th scope="col">Descrpcion</th>
                        <th scope="col">Imagen</th>
                    </tr>
                    </thead>
                    @foreach($objetos2 as $objeto)
                    <tbody>
                        <tr>
                            <td>{{$objeto->nombre_obj}}</td>
                            <td>{{$objeto->estado}}</td>
                            <td><img src="./images/medianas/{{$objeto->ruta}}" height="150" width="150"></td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
	</body>
</html>