<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="/path/to/dist/bootstrap-image-checkbox.css" rel="stylesheet">
</head>
<body>
    <!-- barra superior -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @endguest
                </ul>
            </div>
        </div>
    </nav>            
    @guest             
    @else
        <!-- barra izquierda -->
        <div class="wrapper d-flex align-items-stretch">
            <nav id="sidebar">
                <div class="custom-menu">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary"></button>
                </div>
                <div class="img bg-wrap text-center py-4">
                    <div class="user-logo">
                        @if(file_exists( public_path().'/images/perfiles/'.auth()->user()->id.'.png'))                             <!-- la foto de perfil debe ser png -->
                            <div class="img" style="background-image: url(/images/perfiles/{{auth()->user()->id}}.png);"></div>
                        @else
                            <div class="img" style="background-image: url(/images/perfil_logo.png);"></div>
                        @endif
                        <h3>{{auth()->user()->name}}</h3>
                    </div>
                </div>
                <ul class="list-unstyled components mb-5">
                    <li class="active">
                        <a href="/perfil">Perfil</a>
                        <a href="/misobjetos">Mis objetos</a>
                        <a href="/misIntercambiosAbiertos">Intercambios Recibidos</a>
                        <a href="/misIntercambiosEnviados">Intercambios Enviados</a>
                        <a href="/misIntercambiosCerrados">Intercambios cerrados</a>
                        <a href="/vervaloracion">Valoraciones</a>
                        @if(auth()->user()->tipo==1)
                        <a href="/listaDeUsuarios">Gestionar Usuarios</a>
                        <a href="/altaCSV">Importar/Exportar</a>
                        @endif
                        <a href="javascript: document.getElementById('logout').submit()">Cerrar sesion</a>
                        <form action="{{route('logout')}}" id = 'logout' style = 'display:none' method = 'POST'>
                            @csrf
                        </form>
                    </li>
                </ul>
            </nav>
        @endguest
        <!-- Contenido de la pagina  -->
        <div id="content" class="p-4 p-md-5 pt-5">
            @yield('contenido')
        </div>
    </div>
<!-- javascript para el movimiento del sidebar  -->
    <script src="/js/jquery.min.js"></script>
    <script src="/js/popper.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/main.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js"></script>

</body>

</html>
