@extends('layouts.mizona')
@section('contenido')
	<link rel="stylesheet" href="css/vistaImagenes.css" />
    <link rel="stylesheet" href="css/lc_lightbox.css" />
    <link rel="stylesheet" href="skins/minimal.css" />
    
    <!-- Tienen que estar en el head -->
    <script src="lib/AlloyFinger/alloy_finger.min.js" type="text/javascript"></script>
    <script src="lib/jquery.js" type="text/javascript"></script>
    <script src="js/lc_lightbox.lite.js" type="text/javascript"></script>
    <script src="/js/verImagenes.js" type="text/javascript"></script>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <label for="">Lista de objetos Ajenos</label>
                        <form class="form-inline my-2 my-lg-0" action="{{route('buscaIntercambios')}}" method = "POST">
                            @csrf
                            <input  class="form-control mr-sm-2" type="search" id="buscador" name="buscador" placeholder="Search" aria-label="Search" value="{{$busqueda ?? ''}}">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                    </div>
                    <div class="card-body">
                        @if(session('info'))
                        <div class="alert alert-success"> 
                            {{session('info')}}
                        </div>
						@endif
                        @if(session('error'))
                        <div class="alert alert-danger"> 
                            {{session('error')}}
                        </div>
						@endif
						@foreach($objetos as $objeto)
						<div class="card" style="width: 18rem; display: inline-block;">
							<div class="card-header">
								<a class="elem" href="images/grandes/{{$objeto->ruta}}" data-lcl-thumb="images/pequenas/{{$objeto->ruta}}">
									<span style="background-image: url(images/medianas/{{$objeto->ruta}});"></span>
								</a>
							</div>
							<div class="card-body">
                                <form action="{{route('verObj')}}" method = "GET">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$objeto->id}}">
                                    <button class="btn btn-light btn-lg" type="submit">{{$objeto->nombre_obj}}</button>
                                </form>
                                <p class="card-text">{{$objeto->estado}}</p>
                                <a href="mailto:{{$objeto->email}}?subject=Nuevo intercambio recibido" class="btn btn-primary">Contacto</a><br><br>
                                <form action="{{route('visualizarObjetosIntercambio')}}" method = "POST">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$objeto->id}}">
                                    <button class="btn btn-success" type="submit">Proponer intercambio</button>
                                </form>
							</div>
						</div>
						@endforeach
                    </div>
                    <div class="card-footer">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection
