@extends('layouts.mizona')
@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Lista de intercambios Cerrados
                    </div>
                    <div class="card-body">
                        @if(session('info'))
                        <div class="alert alert-success"> 
                            {{session('info')}}
                        </div>
						@endif
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Numero de objetos solicitado</th>
                                    <th scope="col">Numero de objetos ofrecidos</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
						@foreach($intercambios as $intercambio)
                            <tbody>
                                <tr>
                                    <td >{{$intercambio->updated_at}}</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>
                                    <form action="/verIntercambioCerrado/{{$intercambio->id}}" method = "GET">
                                            @csrf
                                            <button class="btn btn-success" type="submit">Ver</button>
                                        <form action="generarPDF/{{$intercambio->id}}" method = "GET">
                                            @csrf
                                            <button class="btn btn-primary" type="submit">PDF</button>
                                        </form>                                   
                                    </td>
                                </tr>
                            </tbody>
						@endforeach
                        </table>
                    </div>
                    <div class="card-footer">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
