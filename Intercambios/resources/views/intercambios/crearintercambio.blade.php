@extends('layouts.mizona')
@section('contenido')
    <div class="card mb-3" >
        <form id= "insertarIntercambio" action="{{ route('insertarIntercambio')}}" method = "POST">
            @csrf
            <div class="card-header">
                Mis objetos
            </div>
            <div class="card-body">
                @foreach($objetos as $objeto)
                    <div class="custom-control custom-checkbox image-checkbox" style="display: inline-block;">
                        <input type="checkbox" class="custom-control-input" name="check_list[]" id="checkbox.{{$objeto->id}}" value = "{{$objeto->id}}">
                        <label class="custom-control-label" for="checkbox.{{$objeto->id}}">
                            <img src="images/medianas/{{$objeto->ruta}}" width = "150" class="img-fluid" alt="...">
                        </label>
                    </div>
                @endforeach
            </div>
            <div class="card-header">
                Objetos ajenos:
            </div>
            <div class="card-body">
                @foreach($objetosAjenos as $objeto)
                    <div class="custom-control custom-checkbox image-checkbox" style="display: inline-block;">
                        <input type="checkbox" class="custom-control-input" name="check_list_1[]" id="checkbox1.{{$objeto->id}}" value = "{{$objeto->id}}">
                        <label class="custom-control-label" for="checkbox1.{{$objeto->id}}">
                            <img src="images/medianas/{{$objeto->ruta}}" width = "150" class="img-fluid" alt="...">
                        </label>
                    </div>
                @endforeach
            </div>
            <div class="card-footer">
                <input type="hidden" name="id_usr" value= "{{$id_usr}}">
                <input type="submit" class="btn btn-success" name="submit" Value="Enviar"/>
            </div>
        </form>
    </div>
@endsection
