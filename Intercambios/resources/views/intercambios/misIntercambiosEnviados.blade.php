@extends('layouts.mizona')
@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Lista de intercambios Enviados
                    </div>
                    <div class="card-body">
                        @if(session('info'))
                        <div class="alert alert-success"> 
                            {{session('info')}}
                        </div>
						@endif
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Usuario</th>
                                    <th scope="col">Numero de objetos solicitado</th>
                                    <th scope="col">Numero de objetos ofrecidos</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
						@foreach($intercambios as $intercambio)
                            <tbody>
                                <tr>
                                    <td >{{$intercambio->updated_at}}</td>
                                    <td>{{$intercambio->email}}</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>  
                                        <form action="/verIntercambioEnviado/{{$intercambio->id}}" method = "GET">
                                            @csrf
                                            <button class="btn btn-success" type="submit">Ver</button>
                                        </form>
                                        <form action="{{ route('eliminarIntercambio')}}" method = "POST">
                                            @csrf
                                            <input type="hidden" name="id" value= "{{$intercambio->id}}">
                                            <button class="btn btn-danger" type="submit">Eliminar</button>
                                        </form>
                                    </td>
                                </tr>
                            </tbody>
						@endforeach
                        </table>
                    </div>
                    <div class="card-footer">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
