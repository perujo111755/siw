@extends('layouts.mizona')
@section('contenido')

<div class="container">
    <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Opciones
                    </div>
                    <div class="card-body">
                        @if(session('info'))
                        <div class="alert alert-success"> 
                            {{session('info')}}
                        </div>
						@endif
                        @if(session('error'))
                        <div class="alert alert-danger"> 
                            {{session('error')}}
                        </div>
						@endif
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td>
                                    <form action="{{ route('cerrarIntercambio')}}" method = "POST">
                                            @csrf
                                            <input type="hidden" name="id_usr_1" value= "{{$intercambio->id_usr_1}}">
                                            <input type="hidden" name="id_usr_2" value= "{{$intercambio->id_usr_2}}">
                                            <input type="hidden" name="id" value= "{{$intercambio->id}}">
                                            <button class="btn btn-success" type="submit">Aceptar</button>
                                        </form>
                                    </td>
                                    <td>
                                        <form action="{{route('contraoferta')}}" method = "POST">
                                            @csrf
                                            <input type="hidden" name="id_usr_1" value= "{{$intercambio->id_usr_1}}">
                                            <input type="hidden" name="id" value= "{{$intercambio->id}}">
                                            <button class="btn btn-primary" type="submit">Proponer intercambio</button>
                                        </form>                       
                                    </td>
                                    <td>
                                    <form action="{{ route('eliminarIntercambio')}}" method = "POST">
                                            @csrf
                                            <input type="hidden" name="id" value= "{{$intercambio->id}}">
                                            <button class="btn btn-danger" type="submit">Rechazar</button>
                                        </form>
                                    </td>
                                    <td><a href="mailto:{{$usuario->email}}?subject=Duda sobre el intercambio recibido" class="btn btn-primary">Contacto</a><br><br></td>
                                </tr>
                            </theat>
                        </table>
                    </div>
                 </div>
                            
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Oferta realizada por:
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">NOMBRE</th>
                                    <th scope="col">EMAIL</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$usuario->name}}</td>
                                    <td>{{$usuario->email}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        
                    </div>
                </div>
            </div>
        </div>
     
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Objetos que le han ofrecido
                    </div>
                    <div class="card-body">
						@foreach($objetos1 as $objeto)
						<div class="card" style="width: 18rem; display: inline-block;">
							<div class="card-header">
                            <img src="/images/medianas/{{$objeto->ruta}}" style="width: 15rem;">
							</div>
							<div class="card-body">
                                <p class="card-text">{{$objeto->nombre_obj}}</p>
                                <p class="card-text">{{$objeto->estado}}</p>
							</div>
						</div>
						@endforeach
                    </div>
                    <div class="card-footer">                       
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Objetos que le han solicitado
                    </div>
                    <div class="card-body">
						@foreach($objetos2 as $objeto)
						<div class="card" style="width: 18rem; display: inline-block;">
							<div class="card-header">
                            <img src="/images/medianas/{{$objeto->ruta}}" style="width: 15rem;">
							</div>
							<div class="card-body">
                                <p class="card-text">{{$objeto->nombre_obj}}</p>
                                <p class="card-text">{{$objeto->estado}}</p>
							</div>
						</div>
						@endforeach
                    </div>
                    <div class="card-footer">                       
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection