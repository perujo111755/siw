@extends('layouts.mizona')
@section('contenido')

<div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Oferta realizada por:
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">NOMBRE</th>
                                    <th scope="col">EMAIL</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$usuario->name}}</td>
                                    <td>{{$usuario->email}}</td>
                                    <td>
                                        <form action="/generarPDF/{{$intercambio->id}}" method = "GET">
                                            @csrf
                                            <button class="btn btn-primary" type="submit">PDF</button>
                                        </form> 
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Oferta aceptada por:
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">NOMBRE</th>
                                    <th scope="col">EMAIL</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$usuario2->name}}</td>
                                    <td>{{$usuario2->email}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Objetos intercambiados por el usuario 1
                    </div>
                    <div class="card-body">
						@foreach($objetos1 as $objeto)
						<div class="card" style="width: 18rem; display: inline-block;">
							<div class="card-header">
                            <img src="/images/medianas/{{$objeto->ruta}}" style="width: 15rem;">
							</div>
							<div class="card-body">
                                <p class="card-text">{{$objeto->nombre_obj}}</p>
                                <p class="card-text">{{$objeto->estado}}</p>
							</div>
						</div>
						@endforeach
                    </div>
                    <div class="card-footer">                       
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Objetos intercambiados por el usuario 2
                    </div>
                    <div class="card-body">
						@foreach($objetos2 as $objeto)
						<div class="card" style="width: 18rem; display: inline-block;">
							<div class="card-header">
                            <img src="/images/medianas/{{$objeto->ruta}}" style="width: 15rem;">
							</div>
							<div class="card-body">
                                <p class="card-text">{{$objeto->nombre_obj}}</p>
                                <p class="card-text">{{$objeto->estado}}</p>
							</div>
						</div>
						@endforeach
                    </div>
                    <div class="card-footer">                       
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection