@extends('layouts.mizona')
@section('contenido')
<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/valoracion.css">

<form action="{{ route('guardarvaloracion')}}" method="POST">
    @csrf
    <div class="card mb-3" style="width: 100%;">
        <div class="card-header" style="text-align: center;">
            Valoración del intercambio 
        </div>
        @if(session('info'))
            <div class="alert alert-success"> 
                {{session('info')}}
            </div>
		@endif
        @if(session('error'))
            <div class="alert alert-danger"> 
                {{session('error')}}
            </div>
		@endif
        <div class="row no-gutters">
                <div class="col-md-4">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Comentario</span>
                    </div>
                    <textarea id = "comentario" name = "comentario" style="width: 75%;" rows="10" ></textarea>
                </div>
                </div>
                    <div class="col-md-8" style="margin: auto;text-align: center;">
                        <div class="stars">
                            <input type="hidden" name="id_int" value= "{{$id_int}}">
                            <input class="star star-5" id="star-5" type="radio" name="star" value="5"/>
                            <label class="star star-5" for="star-5"></label>
                            <input class="star star-4" id="star-4" type="radio" name="star" value="4"/>
                            <label class="star star-4" for="star-4"></label>
                            <input class="star star-3" id="star-3" type="radio" name="star" value="3"/>
                            <label class="star star-3" for="star-3"></label>
                            <input class="star star-2" id="star-2" type="radio" name="star" value="2"/>
                            <label class="star star-2" for="star-2"></label>
                            <input class="star star-1" id="star-1" type="radio" name="star" value="1"/>
                            <label class="star star-1" for="star-1"></label>
                            <p><button class="btn btn-success" type="submit">Enviar comentario</button></p>
                            <p><a href="/misIntercambiosAbiertos" class="btn btn-danger">No enviar comentario</a></p>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</form>
@endsection
