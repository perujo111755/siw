@extends('layouts.mizona')
@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Valoraciones
                    </div>
                    <div class="card-body">
                        @if(session('info'))
                        <div class="alert alert-success"> 
                            {{session('info')}}
                        </div>
						@endif
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Usuario</th>
                                    <th scope="col">Comentario</th>
                                    <th scope="col">Puntuacion</th>
                                </tr>
                            </thead>
						@foreach($valoraciones as $valoracion)
                            <tbody>
                                <tr>
                                    <td >{{$valoracion->created_at}}</td>
                                    <td>{{$valoracion->id_usr_1}}</td>
                                    <td>{{$valoracion->comentario}}</td>
                                    <td>{{$valoracion->puntuacion}}</td>
                                </tr>
                            </tbody>
						@endforeach
                        </table>
                    </div>
                    <div class="card-footer">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
