@extends('layouts.mizona')
@section('contenido')
    <div class="card mb-3" style="width: 100%;">
        <div class="row no-gutters">
            <div class="col-md-4">
                @if(file_exists( public_path().'/images/perfiles/'.auth()->user()->id.'.png' ))
                    <img src="images/perfiles/{{auth()->user()->id}}.png">
                @else
                    <img src="/images/perfil_logo.png" alt="...">
                @endif
            </div>
                <div class="col-md-8" style="margin: auto;text-align: center;">
                    <div class="card-body">
                        <h5 class="card-title">Nombre: {{auth()->user()->name}}</h5>
                        <p class="card-text">Correo: {{auth()->user()->email}}</p>
                        <p><a href="/editarPerfil" class="btn btn-success">Editar perfil</a></p>
                        <form action="{{ route('eliminarUsuario')}}" method = "POST">
                            @csrf
                            <input type="hidden" name="id" value= "{{auth()->user()->id}}">
                            <button class="btn btn-danger" type="submit">Eliminar</button>
                        </form>
                    </div>
                </div>
             </div>
        </div>
    </div>
@endsection
