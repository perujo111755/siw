@extends('layouts.mizona')
@section('contenido')
    <div class="card mb-3" style="width: 100%;">
        <div class="row no-gutters">
            <div class="col-md-4">
                @if(file_exists( public_path().'/images/perfiles/'.auth()->user()->id.'.png' ))
                    <img src="images/perfiles/{{auth()->user()->id}}.png">
                @else
                    <img src="/images/perfil_logo.png" alt="...">
                @endif
            </div>
                <div class="col-md-8" style="margin: auto;text-align: center;">
                    <div class="card-body">
                        @if(session('error'))
                            <div class="alert alert-danger"> 
                                {{session('error')}}
                            </div>
                        @endif
                        <form id= "editarPerfil" action="{{ route('editarPerfil')}}" method = "POST">
                            @csrf
                            <p>
                            <label for="nombre">Nombre:</label>
                            <input class="card-title" style="text-align:center" id="nombre" name="nombre" type="text" value="{{auth()->user()->name}}">
                            </p>
                            <p>
                                <label for="correo">Correo:</label>
                                <input class="card-text" style="text-align:center" id="correo" name="correo" type="text" value="{{auth()->user()->email}}">
                            </p>
                            <p><a href="/cambiarContraseña" class="btn btn-primary">Cambiar contraseña</a></p>
                            <button class="btn btn-success" type="submit">Guardar cambios</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
