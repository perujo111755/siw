@extends('layouts.mizona')
@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Lista de usuarios
                    </div>
                    <div class="card-body">
                        @if(session('info'))
                        <div class="alert alert-success"> 
                            {{session('info')}}
                        </div>
						@endif
                        @if(session('error'))
                        <div class="alert alert-danger"> 
                            {{session('error')}}
                        </div>
						@endif
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">NOMBRE</th>
                                    <th scope="col">EMAIL</th>
                                    <th scope="col">CONTRASEÑA CIFRADA</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
						@foreach($users as $usuario)
                            <tbody>
                                <tr>
                                    <td >{{$usuario->id}}</td>
                                    <td>{{$usuario->name}}</td>
                                    <td>{{$usuario->email}}</td>
                                    <td>{{$usuario->password}}</td>
                                    <td>
                                        <form action="{{ route('hacerAdmin')}}" method = "POST">
                                            @csrf
                                            <input type="hidden" name="id" value= "{{$usuario->id}}">
                                            @if($usuario->tipo==0)
                                                <button class="btn btn-primary" type="submit">Hacer administrador</button>
                                            @else
                                                <button class="btn btn-success" type="submit">Quitar privilegios</button>
                                            @endif
                                        </form>
                                        <form action="{{ route('eliminarUsuario2')}}" method = "POST">
                                            @csrf
                                            <input type="hidden" name="id" value= "{{$usuario->id}}">
                                            <button class="btn btn-danger" type="submit">Eliminar</button>
                                        </form>
                                    </td>
                                </tr>
                            </tbody>
						@endforeach
                        </table>
                    </div>
                    <div class="card-footer">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection